BIN = poweroff 

LDFLAGS = -static -nostdlib syscall.o $@.s 

all: $(BIN)
	$(MAKE) -C libexec $@ 

install: $(BIN)
	ln -sf poweroff $(DESTDIR)$(PREFIX)/bin/reboot
	install -Dm 755 poweroff $(DESTDIR)$(PREFIX)/bin/poweroff
	$(MAKE) -C libexec $@ 

uninstall:
	unlink $(DESTDIR)$(PREFIX)/bin/reboot
	rm $(DESTDIR)$(PREFIX)/bin/poweroff
	$(MAKE) -C libexec $@ 

clean:
	rm $(BIN) syscall.o
	$(MAKE) -C libexec $@ 


$(BIN): syscall.o
