.global exit ; exit: ; mov $60,%eax ; jmp _syscall

.global _syscall
_syscall:
	movq %r10, %rcx
	syscall
	ret

.global _start
_start:
	mov $0, %rbp
	pop  %rdi
	movq %rsp, %rsi
	call main
	call exit
