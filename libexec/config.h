#define LOGTO    "/dev/tty3"
#define SEEDFILE "/var/lib/init/random-seed"
#define HOSTFILE "/etc/hostname"

#include "common.h"

static const
char * respawn[] = { "/bin/respawn", "-d1", NULL };

static const
struct cmd boot[] = 
{
	{ WAIT,    { "/bin/mount", "-a", NULL } }, 
	{ NOFLAGS, { "/bin/insmod", "/boot/lib/modules/xpad.ko",  NULL } },
	{ NOFLAGS, { "/bin/insmod", "/boot/lib/modules/rtl8821ce.ko", NULL } },
	{ NOFLAGS, { "/bin/smdev", "-s", NULL } },
	{ NOFLAGS, { "/bin/alsactl", "restore", NULL } },
	{ RESPAWN, { "/bin/getty", "/dev/tty1", NULL } },
	{ RESPAWN, { "/bin/getty", "/dev/tty2", NULL } },
	{ RESPAWN, { "/bin/nldev", NULL } },
	{ RESPAWN, { "/bin/wpa_supplicant", "-M", "-c", "/etc/wpa_supplicant.conf", "-s", NULL } },
	{ RESPAWN, { "/bin/sdhcp", "-e", "/usr/libexec/sdhcp/setip", "-f", "-i", "wlan0", NULL } },
};

static const
struct cmd shut[] =
{
	{ WAIT, { "/sbin/killall5", "9", NULL } },
	{ WAIT, { "/sbin/umount", "-a", NULL } },
};
