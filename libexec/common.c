#include "config.h"

int len( const char * msg )
{
	char * p ;
	for( p = msg; *p; ++p );
	return p-msg;
}

void log( const char * msg )
{
	write( 1, "=> ", 4  );
	write( 1, msg  , len(msg) );
	write( 1, "\n" , 1  );
}

void runprgs( struct cmd * cmds, int n )
{
	int tty=open( LOGTO, O_WRONLY);
	close(1);
	close(2);
	dup2(tty, 1);
	dup2(tty, 2);

	for( int i = 0 ; i < n; i++ )
	{
		char * run[16];

		// Fill run
		{
			int k=0;
			if( cmds[i].flags == RESPAWN )
				for( ; run[k]=respawn[k]; ++k);

			for( int j=0; run[j+k]=cmds[i].args[j]; ++j);
		}

		int pid = fork();

		if( pid == 0 )
			execve( run[0], run, NULL );

		if( cmds[i].flags == WAIT )
			wait4( pid, NULL, 0, NULL );
	}
}
