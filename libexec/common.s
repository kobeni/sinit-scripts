.global read   ; read:  ; mov $00,%eax ; jmp _syscall
.global write  ; write: ; mov $01,%eax ; jmp _syscall
.global open   ; open:  ; mov $02,%eax ; jmp _syscall
.global close  ; close: ; mov $03,%eax ; jmp _syscall
.global dup2   ; dup2:  ; mov $33,%eax ; jmp _syscall
.global socket ; socket:; mov $41,%eax ; jmp _syscall
.global fork   ; fork:  ; mov $57,%eax ; jmp _syscall
.global execve ; execve:; mov $59,%eax ; jmp _syscall
.global wait4  ; wait4: ; mov $61,%eax ; jmp _syscall
.global mkdir  ; mkdir: ; mov $83,%eax ; jmp _syscall
.global mount  ; mount: ; mov $165,%eax ; jmp _syscall
.global reboot ; reboot:; mov $169,%eax ; jmp _syscall
.global symlink ; symlink: ; mov $88,%eax ; jmp _syscall
