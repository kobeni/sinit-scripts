#include "config.h"

void misc( void )
{
	log( "Saving seed..." );
	char buf[512];
	int input=open( "/dev/urandom", O_RDONLY );
	int output=open( SEEDFILE, O_WRONLY );
	write( output, &buf[0], read(input, &buf[0], sizeof(buf) ) );
	close( input );
	close( output );
}

int main( int argc, char * argv[] )
{
	misc();
	runprgs( &shut[0], sizeof(shut)/sizeof(shut[0]) );
	return reboot( (argv[1][0] == 'r' )? RB_AUTOBOOT : RB_POWER_OFF );
}
