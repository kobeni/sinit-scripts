#define NOFLAGS 0b00
#define WAIT    0b01
#define RESPAWN 0b10

#include <unistd.h>
#include <fcntl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mount.h>
#include <sys/reboot.h>

struct cmd
{
	char flags;
	char *args[10];
};

void log( const char * );

void runprgs( struct cmd * cmds, int n );
