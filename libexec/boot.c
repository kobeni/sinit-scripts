#include "config.h"
void filesystem( void )
{
	log( "Mounting pseudo filesystem..." );
	mount( "proc", "/proc",        "proc", MS_NOSUID | MS_NOEXEC | MS_NODEV, NULL );
	mount( "sys",   "/sys",       "sysfs", MS_NOSUID | MS_NOEXEC | MS_NODEV, NULL );
	mount( "cache", "/var/cache", "tmpfs", MS_NOSUID | MS_NOEXEC | MS_NODEV, "mode=0777" );
	mount( "tmp",   "/tmp",       "tmpfs", MS_NOSUID | MS_NOEXEC | MS_NODEV, "mode=0777" );
	mount( "run",   "/run",       "tmpfs", MS_NOSUID | MS_NODEV, "mode=0777" );
	mount( "dev",   "/dev",    "devtmpfs", MS_NOSUID, "mode=0755" );

	mkdir( "/dev/pts", 0755 );
	mkdir( "/dev/shm", 0755 );
	mount( "devpts", "/dev/pts", "devpts", MS_NOSUID | MS_NOEXEC, "mode=0620,gid=5" );
	mount( "shm", "/dev/shm", "tmpfs", MS_NOSUID | MS_NODEV, "mode=1777" );

	symlink( "/proc/self/fd", "/dev/fd" );
	symlink( "fd/0", "/dev/stdin" );
	symlink( "fd/1", "/dev/stdout" );
	symlink( "fd/2", "/dev/stderr" );

	mkdir( "/run/lock", 0755 );
}

void loopback( void )
{
	log( "Set up loopback..." );
	int fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
	struct sockaddr_in * addr;

	struct ifreq iface;
	iface.ifr_addr.sa_family = AF_INET;

	const char * ifname = "lo";
	for( int i=0; iface.ifr_name[i]=ifname[i]; ++i);

	addr = &iface.ifr_addr;

	addr->sin_family = AF_INET;
	addr->sin_addr.s_addr = 0x0100007f;
	ioctl(fd, SIOCSIFADDR, &iface);

	addr->sin_family = AF_INET;
	addr->sin_addr.s_addr = 0x0000ff;
	ioctl(fd, SIOCSIFNETMASK, &iface);

	// TODO: set up loopback device correctly
	iface.ifr_flags  |=  ( IFF_UP | IFF_RUNNING | IFF_LOOPBACK );
	ioctl(fd, SIOCGIFFLAGS, &iface);

	close(fd);
}

void hostname( void )
{
	log( "Setting hostname..." );

	char buf[64];
	long result;

	int input=open( HOSTFILE, O_RDONLY);
	int output=open("/proc/sys/kernel/hostname", O_WRONLY);

	while( result=read(input, &buf[0], sizeof(buf))  )
			write(output, &buf[0], result);

	close( input );
	close( output );
}

void urandom( void )
{
	log( "Seeding urandom..." );

	char buf[64];
	long result;

	int output=open("/dev/urandom", O_WRONLY );
	int input;

	if( input=open( SEEDFILE, O_RDONLY ) );
	{
		while( result=read(input, &buf[0], sizeof(buf))  )
			write(output, &buf[0], result);
	}

	close( input );
	close( output );
}

int main( void )
{
	filesystem();
	loopback();
	hostname();
	urandom();
	runprgs( &boot[0], sizeof(boot)/sizeof(boot[0]) );

	return 0;
}

